#!/usr/bin/env python
import unittest

class SuccessfulTest(unittest.TestCase):
    def test_success(self):
        self.assertTrue(True)

    def test_failure(self):
        self.assertTrue(False)

if __name__ == '__main__':
    print("This is a partially successful test.")
    unittest.main()
